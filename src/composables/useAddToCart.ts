import cartService from 'src/services/cart.service';
import notify from 'src/utils/notify';
import messages from 'src/config/messages';
import { ref, Ref } from 'vue';
import { useStore } from 'vuex';
import { storeKey } from 'src/store';

export function useAddToCart(quantity: Ref<number>, product_id: number) {
  const $store = useStore(storeKey);
  const loading = ref(false);

  const addItemToCart = async () => {
    loading.value = true;
    try {
      const data = {
        product_id: product_id,
        quantity: quantity.value,
      };
      await cartService.addItem(data);
      await $store.dispatch('cart/getCarts');
      notify.success(messages.cart.addSuccess, {
        position: 'bottom-right',
      });
    } catch {
      notify.error(messages.cart.addFailed, {
        position: 'bottom-right',
      });
    } finally {
      loading.value = false;
    }
  };

  return {
    loading,
    addItemToCart,
  };
}
