import { Cookies } from 'quasar';
import { ILoginRes } from 'src/interfaces/responses/auth';

const cookieKeys = {
  ACCESS_TOKEN: 'access_token',
  REFRESH_TOKEN: 'refresh_token',
};

// Setters
export const setAuthData = (data: ILoginRes) => {
  Cookies.set(cookieKeys.ACCESS_TOKEN, data.access_token, {
    expires: data.expires_in,
  });
  Cookies.set(cookieKeys.REFRESH_TOKEN, data.refresh_token, {
    expires: data.expires_in,
  });
};

export const deleteAuthData = () => {
  Cookies.remove(cookieKeys.ACCESS_TOKEN);
  Cookies.remove(cookieKeys.REFRESH_TOKEN);
};

// Getters
export const getAccessToken = (): string =>
  Cookies.get(cookieKeys.ACCESS_TOKEN);

export const isAuthen = (): boolean =>
  Cookies.has(cookieKeys.ACCESS_TOKEN) &&
  !!getAccessToken() &&
  getAccessToken().length > 0;

export const getRefreshToken = () => Cookies.get(cookieKeys.REFRESH_TOKEN);
