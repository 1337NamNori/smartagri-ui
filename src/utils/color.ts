const colors = [
  'primary',
  'secondary',
  'accent',
  'dark',
  'positive',
  'negative',
  'info',
  'warning',
];

export const randomColor = () => {
  const colorsArrayLength = colors.length;
  const randomIndex = Math.floor(Math.random() * colorsArrayLength);
  return colors[randomIndex];
};
