import { Notify, QNotifyCreateOptions } from 'quasar';

const createNotify =
  (color: string) => (message: string, options?: QNotifyCreateOptions) =>
    Notify.create({
      color: color,
      message: message,
      position: 'top-right',
      ...options,
    });

const successNotify = createNotify('positive');
const errorNotify = createNotify('negative');
const warningNotify = createNotify('warning');
const infoNotify = createNotify('info');

export default {
  success: successNotify,
  error: errorNotify,
  warning: warningNotify,
  info: infoNotify,
};
