export default {
  formatPrice(price: number): string {
    return price.toLocaleString('it-IT');
  },
  formatPriceWithCurrency(price: number): string {
    return price.toLocaleString('it-IT', {
      style: 'currency',
      currency: 'VND',
    });
  },
};
