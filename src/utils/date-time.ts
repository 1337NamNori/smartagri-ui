import moment from 'moment';

export default {
  formatDate(date: Date | string = new Date(), format = 'DD/MM/YYYY'): string {
    if (typeof date === 'string') date = new Date(date);
    return moment(date).format(format);
  },
};
