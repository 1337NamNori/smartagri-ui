import messages from 'src/config/messages';

const EMAIL_REGEX =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const required = (val: string) =>
  (val && val.length > 0) || messages.validate.required;

export const email = (val: string) =>
  (val && EMAIL_REGEX.test(val)) || messages.validate.email;

export const min = (length: number) => (val: string) =>
  (val && val.length >= length) || messages.validate.min(length);

export const max = (length: number) => (val: string) =>
  (val && val.length <= length) || messages.validate.max(length);

export const minMax = (minLength: number, maxLength: number) => (val: string) =>
  (val && val.length >= minLength && val.length <= maxLength) ||
  messages.validate.minMax(minLength, maxLength);
