import configRoutes from 'src/config/routes';
import { RouteRecordRaw } from 'vue-router';
import { Layouts } from 'src/config/layouts';

const configRouteArray = Object.values(configRoutes);
const routes: RouteRecordRaw[] = [];

Layouts.map((layout) => {
  const children = configRouteArray.filter(
    (route) => route.layout === layout.name
  );
  routes.push({
    path: '/',
    component: layout.component,
    children,
  });
});

configRouteArray
  .filter((route) => route.layout === undefined)
  .map((route) => {
    routes.push(route);
  });

export default routes;
