export const toQSelectOptions = (
  array: { [key: string]: unknown }[],
  labelField: string,
  valueField: string
) => {
  return array.map((item) => ({
    label: item[labelField],
    value: item[valueField],
  }));
};
