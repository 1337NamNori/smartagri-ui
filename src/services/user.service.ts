import { api } from 'src/boot/axios';
import { IBaseRes } from 'src/interfaces/responses/base';
import { IUserInfoRes } from 'src/interfaces/responses/user';

export default {
  getUserInfo(id: number): Promise<IBaseRes<IUserInfoRes>> {
    const url = 'api/users/' + id;
    return api.get(url);
  },
};
