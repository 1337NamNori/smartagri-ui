import { api } from 'src/boot/axios';
import { IProductListQuery } from 'src/interfaces/requests/product';
import { IBaseRes, IPaginationRes } from 'src/interfaces/responses/base';
import { IProductRes } from 'src/interfaces/responses/product';
import { IProductFeedbackRes } from 'src/interfaces/responses/feedback';

export default {
  getList: (
    params: IProductListQuery
  ): Promise<IPaginationRes<IProductRes[]>> => {
    const url = '/api/products';
    return api.get(url, { params });
  },
  getTrendings: (): Promise<IBaseRes<IProductRes[]>> => {
    const url = '/api/products/trending';
    return api.get(url);
  },
  getDetail: (id: number): Promise<IBaseRes<IProductRes>> => {
    const url = `/api/products/${id}`;
    return api.get(url);
  },
  getFeedbacks: (id: number): Promise<IBaseRes<IProductFeedbackRes[]>> => {
    const url = `/api/products/${id}/feedbacks`;
    return api.get(url);
  },
};
