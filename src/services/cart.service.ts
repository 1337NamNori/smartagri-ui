import { api } from 'src/boot/axios';
import { IBaseRes } from 'src/interfaces/responses/base';
import { ICartRes } from 'src/interfaces/responses/cart';
import { ICartReq } from 'src/interfaces/requests/cart';
import apiUrl from 'src/config/api-url';

export default {
  getList: (): Promise<IBaseRes<ICartRes[]>> => {
    const url = '/api/me/carts';
    return api.get(url);
  },
  addItem: (data: ICartReq): Promise<void> => {
    return api.post(apiUrl.carts.ADD, { data });
  },
  updateQuantity: (cartId: number, quantity: number): Promise<void> => {
    const url = apiUrl.carts.UPDATE(cartId);
    return api.put(url, { data: { quantity } });
  },
  deleteItem: (cartId: number): Promise<void> => {
    const url = apiUrl.carts.DELETE(cartId);
    return api.delete(url);
  },
};
