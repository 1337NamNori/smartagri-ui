import { api } from 'src/boot/axios';
import { ILoginReq } from 'src/interfaces/requests/auth';
import { ILoginRes, IUserRes } from 'src/interfaces/responses/auth';
import { IBaseRes } from 'src/interfaces/responses/base';

export default {
  login: (data: ILoginReq): Promise<IBaseRes<ILoginRes>> => {
    const url = '/api/auth/login';
    return api.post(url, {
      data: {
        username: data.email,
        password: data.password,
        scope: '*',
        grant_type: 'password',
        client_id: process.env.AUTH_CLIENT_ID,
        client_secret: process.env.AUTH_CLIENT_SECRET,
      },
    });
  },

  getAccountInfo: (): Promise<IBaseRes<IUserRes>> => {
    const url = '/api/me';
    return api.get(url);
  },
};
