export default {
  login: {
    success: 'Đăng nhập thành công',
    failed: 'Đăng nhập thất bại',
  },
  validate: {
    required: 'Trường này là bắt buộc',
    email: 'Nhập email hợp lệ',
    max: (length: number) => `Nhập trường này với nhiều nhất ${length} ký tự`,
    min: (length: number) => `Nhập trường này với ít nhất ${length} ký tự`,
    minMax: (minLength: number, maxLength: number) =>
      `Nhập trường này với ít nhất ${minLength} ký tự và nhiều nhất ${maxLength} ký tự`,
  },
  cart: {
    addSuccess: 'Thêm vào giỏ hàng thành công. Hãy mua sắm tiếp nào.',
    addFailed: 'Đã có lỗi xảy ra khi thêm vào giỏ hàng. Vui lòng thử lại sau.',
    deleteSuccess: 'Đã xóa thành công khỏi giỏ hàng.',
    deleteFailed: 'Xóa thất bại. Vui lòng thử lại sau.',
  },
};
