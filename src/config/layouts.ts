export const LAYOUT = {
  MAIN: 'MainLayout',
  HEADER_ONLY: 'HeaderOnly',
};

export const Layouts = [
  {
    name: LAYOUT.MAIN,
    component: () => import('layouts/MainLayout.vue'),
  },
  {
    name: LAYOUT.HEADER_ONLY,
    component: () => import('layouts/HeaderOnly.vue'),
  },
];
