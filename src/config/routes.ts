import { LAYOUT } from 'src/config/layouts';

type Route = {
  path: string;
  name: string;
  layout?: string;
  meta?: Record<string, unknown>;
  component: () => Promise<unknown>;
};

export default {
  register: {
    path: '/register',
    name: 'register',
    component: () => import('pages/Register.vue'),
    meta: {
      requiresGuest: true,
    },
  },
  login: {
    path: '/login',
    name: 'login',
    component: () => import('pages/Login.vue'),
    meta: {
      requiresGuest: true,
    },
  },
  cart: {
    path: '/carts',
    name: 'carts-list',
    component: () => import('pages/me/Cart.vue'),
    layout: LAYOUT.HEADER_ONLY,
  },
  myProfile: {
    path: '/me/profile',
    name: 'me-profile',
    component: () => import('pages/me/Profile.vue'),
    layout: LAYOUT.HEADER_ONLY,
  },
  products: {
    path: '/products',
    name: 'products-list',
    component: () => import('pages/products/Index.vue'),
    layout: LAYOUT.MAIN,
  },
  productDetail: {
    path: '/products/:id',
    name: 'product-detail',
    component: () => import('pages/products/Detail.vue'),
    layout: LAYOUT.MAIN,
  },
  shopDetail: {
    path: '/shops/:id',
    name: 'shop-detail',
    component: () => import('pages/shops/Detail.vue'),
    layout: LAYOUT.MAIN,
  },
  home: {
    path: '/',
    name: 'home',
    component: () => import('pages/IndexPage.vue'),
    layout: LAYOUT.MAIN,
  },
} as Record<string, Route>;
