const apiUrl = {
  carts: {
    ADD: 'api/carts',
    UPDATE: (id: number) => 'api/carts/' + id,
    DELETE: (id: number) => 'api/carts/' + id,
  },
  me: {
    CARTS: 'api/me/carts',
  },
};

export default apiUrl;
