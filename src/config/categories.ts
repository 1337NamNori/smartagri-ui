export default [
  {
    id: 1,
    name: 'Khác',
    image: '/images/categories/1.png',
  },
  {
    id: 2,
    name: 'Rau',
    image: '/images/categories/2.png',
  },
  {
    id: 3,
    name: 'Trái cây',
    image: '/images/categories/3.png',
  },
  {
    id: 4,
    name: 'Cây cảnh',
    image: '/images/categories/4.png',
  },
  {
    id: 5,
    name: 'Vật tư nông nghiệp',
    image: '/images/categories/5.png',
  },
  {
    id: 6,
    name: 'Hạt giống',
    image: '/images/categories/6.png',
  },
];
