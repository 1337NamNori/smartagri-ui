import { MutationTree } from 'vuex';

import { ProductStateInterface } from './state';
import { IProductListQuery } from 'src/interfaces/requests/product';

const mutation: MutationTree<ProductStateInterface> = {
  createQuery(state, query: IProductListQuery) {
    state.query = query;
  },
  updateQuery(state, query: IProductListQuery) {
    state.query = {
      ...state.query,
      ...query,
    };
  },
  clearSortQuery(state) {
    delete state.query.sort;
  },
  clearColQuery(state) {
    delete state.query.col;
  },
  clearNameQuery(state) {
    delete state.query.name;
  },
  clearCategoryQuery(state) {
    delete state.query.category;
  },
};

export default mutation;
