import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { ProductStateInterface } from './state';

const actions: ActionTree<ProductStateInterface, StateInterface> = {};

export default actions;
