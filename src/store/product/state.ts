import { IProductListQuery } from 'src/interfaces/requests/product';

export interface ProductStateInterface {
  query: IProductListQuery;
}

function state(): ProductStateInterface {
  return {
    query: {
      page: 1,
    },
  };
}

export default state;
