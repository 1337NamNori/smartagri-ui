import { ICartRes } from 'src/interfaces/responses/cart';

export interface CartStateInterface {
  carts: ICartRes[];
}

function state(): CartStateInterface {
  return {
    carts: [],
  };
}

export default state;
