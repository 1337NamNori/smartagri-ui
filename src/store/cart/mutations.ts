import { MutationTree } from 'vuex';

import { CartStateInterface } from './state';
import { ICartRes } from 'src/interfaces/responses/cart';

const mutation: MutationTree<CartStateInterface> = {
  updateCarts(state, carts: ICartRes[]) {
    state.carts = carts;
  },
};

export default mutation;
