import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { CartStateInterface } from './state';
import cartService from 'src/services/cart.service';

const actions: ActionTree<CartStateInterface, StateInterface> = {
  async getCarts({ commit }) {
    try {
      const res = await cartService.getList();
      commit('updateCarts', res.data);
    } catch {}
  },
};

export default actions;
