import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { AuthStateInterface } from './state';

const getters: GetterTree<AuthStateInterface, StateInterface> = {
  getFullName: (state) => state.user.first_name + ' ' + state.user.last_name,
};

export default getters;
