import { ActionTree } from 'vuex';

import { StateInterface } from '../index';
import { AuthStateInterface } from './state';
import * as storage from 'src/utils/storage';
import authService from 'src/services/auth.service';
import { ILoginReq } from 'src/interfaces/requests/auth';

const actions: ActionTree<AuthStateInterface, StateInterface> = {
  async login({ commit }, formData: ILoginReq) {
    try {
      const res = await authService.login(formData);
      storage.setAuthData(res.data);
      return commit('updateAuth');
    } catch (error) {
      throw error;
    }
  },
  async getAccountInfo({ commit }) {
    try {
      const res = await authService.getAccountInfo();
      return commit('updateAuthUser', res.data);
    } catch (error) {
      throw error;
    }
  },
  logout({ commit }) {
    storage.deleteAuthData();
    commit('updateAuth');
  },
};

export default actions;
