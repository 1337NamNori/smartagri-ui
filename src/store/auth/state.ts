import { IUserRes } from 'src/interfaces/responses/auth';

export interface AuthStateInterface {
  isAuthen: boolean;
  user: IUserRes;
}

function state(): AuthStateInterface {
  return {
    isAuthen: false,
    user: {} as IUserRes,
  };
}

export default state;
