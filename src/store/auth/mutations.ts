import { MutationTree } from 'vuex';

import { AuthStateInterface } from './state';
import * as storage from 'src/utils/storage';
import { IUserRes } from 'src/interfaces/responses/auth';

const mutation: MutationTree<AuthStateInterface> = {
  updateAuth(state) {
    state.isAuthen = storage.isAuthen();
  },
  updateAuthUser(state, data: IUserRes) {
    state.user = data;
  },
};

export default mutation;
