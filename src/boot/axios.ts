import { boot } from 'quasar/wrappers';
import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

import * as storage from 'src/utils/storage';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $axios: AxiosInstance;
  }
}

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Accept'] = 'application/json';

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
const apiRequest =
  (method: AxiosRequestConfig['method']) =>
  <T>(url: string, configs?: AxiosRequestConfig, multipart = false) => {
    const accessToken = storage.getAccessToken();
    const headers = {} as AxiosRequestConfig['headers'];
    if (accessToken) {
      headers['Authorization'] = 'Bearer ' + accessToken;
    }

    if (multipart) {
      headers['Content-Type'] = 'multipart/form-data';
    }

    return new Promise<T>((resolve, reject) => {
      axios({
        baseURL: process.env.BASE_API_URL,
        method: method,
        url: url,
        ...configs,
        headers,
      })
        .then((response) => resolve(response.data))
        .catch((error) => {
          if (error.response.status === 401) {
            storage.deleteAuthData();
            window.location.href = '/login';
          }
          return reject(error);
        });
    });
  };

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios;
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file
});

export const api = {
  get: apiRequest('GET'),
  post: apiRequest('POST'),
  put: apiRequest('PUT'),
  patch: apiRequest('PATCH'),
  delete: apiRequest('DELETE'),
};
