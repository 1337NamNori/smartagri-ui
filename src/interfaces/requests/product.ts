export type IProductListQuery = {
  page?: number;
  limit?: number;
  name?: string;
  col?: 'price' | 'created_at';
  sort?: 'DESC' | 'ASC';
  category?: number;
};
