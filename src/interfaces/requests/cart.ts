export interface ICartReq {
  product_id: number;
  quantity: number;
}
