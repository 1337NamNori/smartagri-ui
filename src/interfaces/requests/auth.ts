export type ILoginReq = {
  email: string;
  password: string;
};
