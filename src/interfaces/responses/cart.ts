import { IProductRes } from './product';

export type ICartRes = {
  id: number;
  user_id: number;
  product_id: number;
  quantity: number;
  created_at: Date;
  product: IProductRes;
};
