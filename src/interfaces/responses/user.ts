export type IUserInfoRes = {
  id: number;
  role: number;
  role_name: string;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
  created_at: Date;
};
