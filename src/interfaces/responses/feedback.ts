export type IFeedbackImageRes = {
  id: number;
  feedback_id: number;
  path: string;
  created_at: Date;
};

export type IProductFeedbackRes = {
  id: number;
  order_product_id: number;
  rate: number;
  comment: string;
  created_at: Date;
  images: IFeedbackImageRes[];
  user_name: string;
  user_avatar: string;
};
