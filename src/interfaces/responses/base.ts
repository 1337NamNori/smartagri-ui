export interface IBaseRes<T> {
  code: number;
  data: T;
}

export interface IPaginationRes<T> extends IBaseRes<T> {
  current_page: number;
  last_page: number;
  per_page: number;
  total: number;
}
