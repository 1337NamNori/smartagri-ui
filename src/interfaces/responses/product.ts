export type IProductImageRes = {
  id: number;
  product_id: number;
  path: string;
  created_at: Date;
};

export type IProductRes = {
  id: number;
  user_id: number;
  category: number;
  category_name: string;
  name: string;
  description: string;
  price: number;
  count_in_stock: number;
  created_at: Date;
  sold_number: number;
  sold_number_past_week: number;
  rate: string;
  images: IProductImageRes[];
};
