export type IUserRes = {
  id: number;
  role: number;
  role_name: string;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
};

export type ILoginRes = {
  access_token: string;
  refresh_token: string;
  expires_in: number;
  user: IUserRes;
};
